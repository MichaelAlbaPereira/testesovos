package pages.WEB;

import Base.BaseUtil;
import org.junit.Assume;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import steps.Hook;
import static org.junit.Assert.fail;

import static Base.BaseUtil.Driver;
import static Base.BaseUtil.VerificaObjetoExistente;

public class ProductsPage {
    public ProductsPage(WebDriver driver) {
        PageFactory.initElements(driver, this);
    }

    public static ProductsPage action() {
        return new ProductsPage(Driver);
    }

    public ProductsPage SelectProduct() {

        try {
            Thread.sleep(2000);

            VerificaObjetoExistente(null, Driver.findElements(By.className("sg-col-inner")).get(3), true, 30);

            WebElement product = Driver.findElements(By.className("sg-col-inner")).get(3);
            String TextProduct = product.getText();

            //Click Selected Product
            product.click();

            BaseUtil.action()
                    .Report("INFO", false, Hook.nomeCenario, false, null, "Select the Product: " + TextProduct, false);

            if(Hook.nomeCenario.contains("008 - Order flow validation")) {
                Thread.sleep(2000);
                BaseUtil.action()
                        .Report("PASS", false, Hook.nomeCenario, false, null, "Sucess to Validation Order flow", false);
                fail();
            }
        } catch (Exception e) {
            BaseUtil.action()
                    .Report("FAIL", false, Hook.nomeCenario, false, null, "Fail to Select the Product: " + e.getMessage(), false);

        }
        return new ProductsPage(Driver);
    }

    public ProductsPage SelectQtyProduct(String qty) {

        try {
            // Cart Qty
            BaseUtil.action().PreencheValorCampoSetSelectButton(By.name("quantity"), null, qty, 30);

            BaseUtil.action()
                    .Report("INFO", false, Hook.nomeCenario, false, null, "Select the Qty Product: " + qty, false);

        } catch (Exception e) {
            BaseUtil.action()
                    .Report("FAIL", false, Hook.nomeCenario, false, null, "Fail to Select the qty: " + qty + " Product: " + e.getMessage(), false);
        }
        return new ProductsPage(Driver);
    }

    public ProductsPage AddProductCart() {

        try {
            //Add to Cart Button
            BaseUtil.action().PreencheValorCampoSetSelectButton(By.id("add-to-cart-button"), null, null, 30);

            BaseUtil.action()
                    .Report("INFO", false, Hook.nomeCenario, false, null, "Click Add to Cart Button", false);

        } catch (Exception e) {
            BaseUtil.action()
                    .Report("FAIL", false, Hook.nomeCenario, false, null, "Fail to Click Add to Cart Button" + e.getMessage(), false);
        }
        return new ProductsPage(Driver);
    }

    public ProductsPage VerifyCart() {

        try {
            Thread.sleep(2000);

            //Add to Cart Button
            BaseUtil.action().PreencheValorCampoSetSelectButton(By.id("nav-cart-count"), null, null, 30);

            BaseUtil.action()
                    .Report("INFO", false, Hook.nomeCenario, false, null, "Click Cart Button", false);

            if(Hook.nomeCenario.contains("009 - Validation of the purchase process (cart)")) {
                BaseUtil.action()
                        .Report("PASS", false, Hook.nomeCenario, false, null, "Sucess to Validation Cart", false);
                fail();
            }
        } catch (Exception e) {
            BaseUtil.action()
                    .Report("FAIL", false, Hook.nomeCenario, false, null, "Fail to Click Cart Button" + e.getMessage(), false);
        }
        return new ProductsPage(Driver);
    }


}
