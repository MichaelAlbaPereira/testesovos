package pages.WEB;

import Base.BaseUtil;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import steps.Hook;

import java.util.List;

import static Base.BaseUtil.Driver;
import static Base.BaseUtil.VerificaObjetoExistente;

public class DropDownMenuPage {
    public DropDownMenuPage(WebDriver driver) {
        PageFactory.initElements(driver, this);
    }

    public static DropDownMenuPage action() {
        return new DropDownMenuPage(Driver);
    }

    @FindBy(how = How.ID, using = "nav-hamburger-menu")
    public WebElement menuNavBarDropDown;

    public DropDownMenuPage VerifyChangeAdress() throws InterruptedException {

        if (VerificaObjetoExistente(By.className("a-button-inner"), null, false, 10)) {
            Driver.findElements(By.className("a-button-inner")).get(0).click();
            BaseUtil.action()
                    .Report("INFO", false, Hook.nomeCenario, false, null, "We're showing you items that ship to BR. To see items that ship to a different country, change your delivery address. | Option: Don't Change", false);
        }
        return new DropDownMenuPage(Driver);
    }

    public DropDownMenuPage ClickMenuDropDown() throws InterruptedException {
        try {
            Actions action = new Actions(Driver);
            action.click(menuNavBarDropDown).build().perform();

            BaseUtil.action()
                    .Report("INFO", false, Hook.nomeCenario, false, null, "Click in Navbar Menu", false);
        } catch (Exception e) {
            BaseUtil.action()
                    .Report("FAIL", false, Hook.nomeCenario, false, null, "Fail to Click in Navbar Menu", false);

        }
        return new DropDownMenuPage(Driver);
    }

    public DropDownMenuPage SelectItemMenu(String option) {
        try {
            Thread.sleep(2000);

            List<WebElement> listItensMenu = Driver.findElements(By.className("hmenu-visible")).get(1).findElements(By.className("hmenu-item"));

            for (WebElement itemList : listItensMenu)
                if (itemList.getText().equals(option)) {
                    itemList.click();
                    BaseUtil.action()
                            .Report("INFO", false, Hook.nomeCenario, false, null, "Select: " + option + " in Navbar Menu", false);
                    break;
                }
        } catch (Exception e) {
            BaseUtil.action()
                    .Report("FAIL", false, Hook.nomeCenario, false, null, "Fail to Select: " + option + " in Navbar Menu", false);

        }
        return new DropDownMenuPage(Driver);
    }
}
