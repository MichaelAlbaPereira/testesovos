package pages.API;

import Base.BaseUtil;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import steps.Hook;

public class APIGETMethodPage {

    public static APIGETMethodPage action() {
        return new APIGETMethodPage();
    }

    public static Response result = null;
    public static String responseText = null;

    public APIGETMethodPage GetMethod(String cenary) {
        RestAssured.baseURI = "https://mocki.io";
        String tracker = "/v1/19430625-2b1c-492a-925f-8b4921964ac3";

        result = APIPage.action().GETMethod(tracker);
        responseText = result.prettyPrint();

        BaseUtil.action()
                .Report("INFO", false, Hook.nomeCenario, false, null, "URI: " + "https://mocki.io" + tracker, false);

        if (APIPage.action().ValidateStatusCode(result)) {
            BaseUtil.action()
                    .Report("PASS", false, Hook.nomeCenario, false, null, "Status Code: " + result.getStatusCode(), false);

            BaseUtil.action()
                    .Report("INFO", false, Hook.nomeCenario, false, null, "<pre> Response: " + responseText + "</pre>", false);

            switch (cenary) {
                case "001 - API - return notifications for the following countries: BR, AR":
                    if (ValidateCenaryPage.VerifyCountries(responseText))
                        BaseUtil.action()
                                .Report("PASS", false, Hook.nomeCenario, false, null, "Countries found with your notifications", false);
                    break;
                case "002 - API - perPage value should correspond to the number of notifications retrieved":
                    if (ValidateCenaryPage.VerifyNotificationsCount(responseText))
                        BaseUtil.action()
                                .Report("PASS", false, Hook.nomeCenario, false, null, "perPage value should correspond to the number of notifications", false);
                    break;
                case "003 - API - content of notifications should be a xml encoded on Base64":
                    if (ValidateCenaryPage.VerifyEncode64(responseText))
                        BaseUtil.action()
                                .Report("PASS", false, Hook.nomeCenario, false, null, "notifications are Base64 encoded xml", false);
                    break;
                case "004 - API - notificationId should be a valid GUID":
                    if (ValidateCenaryPage.VerifyGUID(responseText))
                        BaseUtil.action()
                                .Report("PASS", false, Hook.nomeCenario, false, null, "GUID Cenary finished", false);
                    break;
                case "005 - API - notificationId should correspond to ID inside content xml document":
                    if (ValidateCenaryPage.VerifyFindNotificationIdInsideDocument(responseText))
                        BaseUtil.action()
                                .Report("PASS", false, Hook.nomeCenario, false, null, "notificationId should correspond to ID inside content xml document", false);
                    break;
                case "006 - API - 200 notifications should have Document Authorized on StatusReason and Document authorized successfully on Text fields inside content xml document":
                    if (ValidateCenaryPage.VerifyMessageStatusCode200(responseText))
                        BaseUtil.action()
                                .Report("PASS", false, Hook.nomeCenario, false, null, "status code 200 messages finished ", false);
                    break;
                case "007 - API - 400 notifications should have Document Rejected on StatusReason and Document was rejected by tax authority on Text fields inside content xml document":
                    if (ValidateCenaryPage.VerifyMessageStatusCode400(responseText))
                        BaseUtil.action()
                                .Report("PASS", false, Hook.nomeCenario, false, null, "status code 400 messages finished ", false);
                    break;
                case "008 - API - Automation should display a warn in case of any rejected notification":
                    if (ValidateCenaryPage.VerifyMessageStatusCode400DisplayWarm(responseText))
                        BaseUtil.action()
                                .Report("PASS", false, Hook.nomeCenario, false, null, "status code 400 messages Warm finished ", false);
                    break;
            }

        } else
            BaseUtil.action()
                    .Report("FAIL", false, Hook.nomeCenario, false, null, "Status Code URI + tracker: " + result.getStatusCode() + "| Response: " + result.prettyPrint(), false);


        return new APIGETMethodPage();
    }
}
