package pages.API;

import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public class APIPage {
    public static APIPage action() {
        return new APIPage();
    }

    public Response POSTMethod(String URI, String body) {
        RequestSpecification request = RestAssured.given();
        Response response = request
                .header("content-type", "text/plain")
                .header("APIM-debug", true)
                .body(body)
                .post(URI)
                .then()
                .extract().response();

        return response;
    }

    public Response GETMethod(String tracker) {
        RequestSpecification request = RestAssured.given();
        Response response = request
                .header("content-type", "application/json")
                .get(tracker)
                .then()
                .extract()
                .response();

        return response;
    }

    public boolean ValidateStatusCode(Response response) {
        switch (response.getStatusCode()) {
            case 200:
                //OK
                return true;
            case 201:
                //Criado
                return true;
            case 202:
                //Aceito
                return true;
            case 204:
                //Sem Conteudo - Qualquer metainformação nova ou atualizada deve ser aplicada ao documento atualmente na visão dinâmica do agente do usuário.
                return false;
            case 301:
                //Movido Permanentemente - Precisa especificar a nova URI.
                return false;
            case 302:
                //Encontrado - especificam que o cliente não tem permissão para alterar o método na solicitação redirecionada.
                return false;
            case 303:
                //Ver Outro - indica que um recurso de controlador concluiu seu trabalho, mas em vez de enviar um corpo de resposta potencialmente indesejado, ele envia ao cliente o URI de um recurso de resposta.
                return false;
            case 304:
                //Ver Outro - quando o recurso não foi modificado desde a versão especificada pelos cabeçalhos de solicitação If-Modified-Since ou If-None-Match.
                return false;
            case 404:
                return false;
            case 503:
                return false;
            default:
                return false;
        }
    }

}