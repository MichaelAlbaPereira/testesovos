package steps.API;

import io.cucumber.java.en.Given;
import pages.API.APIGETMethodPage;

public class APIGetStep {
    public static APIGetStep action() {
        return new APIGetStep();
    }

    @Given("I realizing a Get Method and validate a {string}")
    public void iRealizingAGetMethod(String cenary) {
        APIGETMethodPage.action().GetMethod(cenary);
    }
}
