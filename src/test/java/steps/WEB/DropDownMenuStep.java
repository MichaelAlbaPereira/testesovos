package steps.WEB;

import Base.BaseUtil;
import io.cucumber.java.en.And;
import pages.WEB.DropDownMenuPage;

public class DropDownMenuStep extends BaseUtil {
    public static DropDownMenuStep action() {
        return new DropDownMenuStep();
    }

    @And("I navigate through the drop-down menu with {string}{string}")
    public void iNavigateThroughTheDropDownMenuWithAndSelectThe(String option, String subOption) throws InterruptedException {

        DropDownMenuPage.action()
                .VerifyChangeAdress()
                .ClickMenuDropDown()
                .SelectItemMenu(option)
                .SelectItemMenu(subOption);
    }
}
