package steps.WEB;

import io.cucumber.java.en.And;
import pages.WEB.ProductsPage;

public class ProductsStep {
    public static ProductsStep action() {
        return new ProductsStep();
    }

    @And("I select a product")
    public void iSelectAProduct() {
        ProductsPage.action()
                .SelectProduct();

    }

    @And("I add to cart {string}")
    public void iAddToCart(String qtyProducts) {
        ProductsPage.action()
                .SelectQtyProduct(qtyProducts)
                .AddProductCart()
                .VerifyCart();

    }
}
