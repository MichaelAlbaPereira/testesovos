@MichaelAlba @testeSovos
Feature: SovosTest

  Scenario Outline: "<cenary>"
    Given I realizing a Get Method and validate a "<cenary>"

    Examples:
      | cenary                                                                                                                                                            |
      | 001 - API - return notifications for the following countries: BR, AR                                                                                              |
      | 002 - API - perPage value should correspond to the number of notifications retrieved                                                                              |
      | 003 - API - content of notifications should be a xml encoded on Base64                                                                                            |
      | 004 - API - notificationId should be a valid GUID                                                                                                                 |
      | 005 - API - notificationId should correspond to ID inside content xml document                                                                                    |
      | 006 - API - 200 notifications should have Document Authorized on StatusReason and Document authorized successfully on Text fields inside content xml document     |
      | 007 - API - 400 notifications should have Document Rejected on StatusReason and Document was rejected by tax authority on Text fields inside content xml document |
      | 008 - API - Automation should display a warn in case of any rejected notification                                                                                 |





