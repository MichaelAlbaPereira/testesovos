@MichaelAlba @testeSovos
Feature: SovosTest

  Scenario Outline: "<cenary>"
    Given I navigate to URL
    And I navigate through the drop-down menu with "<optionDropDownMenu>""<subOptionDropDownMenu>"
    And I select a product
    And I add to cart "<qtyProducts>"
    #Then I verify the results

    #Testes 01 até o 06 necessitam de uma conta real na amazon, não realizei por causa de já possuir uma conta associada ao meu número de telefone.
    Examples:
      | cenary                                          | optionDropDownMenu | subOptionDropDownMenu | qtyProducts |
     #| 001 - Validation of coupons and discounts       | Computers          | Monitors              | 2           |
     #| 002 - Validation of address information         | Computers          | Monitors              | 1           |
     #| 003 - Inventory data validation                 | Computers          | Monitors              | 1           |
     #| 004 - Customer data validation                  | Computers          | Monitors              | 1           |
     #| 005 - Payment flow validation                   | Computers          | Monitors              | 1           |
     #| 006 - Freight calculation validation            | Computers          | Monitors              | 1           |
      | 007 - Validation of installment forms           | Computers          | Monitors              | 1           |
      | 008 - Order flow validation                     | Computers          | Monitors              | 1           |
      | 009 - Validation of the purchase process (cart) | Computers          | Monitors              | 2           |






